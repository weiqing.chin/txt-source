猛力痛毆窗戶的強烈風聲，打破了春雪淺淺的睡眠。

一片漆黑之中，他蒙著棉被仔細傾聽，就能聽見無數水滴乘著吹來的風，霹哩啪啦打在玻璃窗上的聲響。看樣子在他不知不覺間，外面已經下起了雨。

相信一夜之間，這陣風雨就會將大樓庭園內的櫻花吹得七零八落，但無論這些季節遞嬗的現象是否發生，春天對春雪來說始終是個憂郁的季節。

理由有二。首先是濕度跟氣溫都會上升。春雪的汗腺比常人加倍發達，就連攝氏二十五度左右的氣溫，都足以讓他的額頭開始冒汗。

另一個理由則是升上了新學年。維持良久的苦難生活總算過去，春雪好不容易算是在班上爭取到了一個無關痛癢的位置，卻在這時被迫重新分班，這對他來說根本只是在找碴。得面對一群陌生的同學，從下PING指令試探彼此間的距離開始來往，實在令他覺得天旋地轉。

至少要把春假最後的幾個小時延長一點，這應該不會遭天譴。

想到這裡，春雪伸手摸索，從床邊梯子踏板上抓起了神經連結裝置，將其配戴在脖子上並開啟電源，環扣就在一陣輕輕的驅動聲中往內側擺去。接著開機程序敔動，在檢查完裝置與五感接觸無誤的同時，半透明的虛擬桌面便在眼前展開。

春雪先朝顯示在視野右下方的時刻「2047／04／08 AMO1：22」一瞥、接著深深吸氣，開口念出：

「超頻……」

連線。

就在他即將念出這句神奇魔咒之際。

隨著一聲輕巧的來電提示音響起，語音通話的呼叫提示開始閃爍藍色的光芒。

春雪反射性地用右手指尖觸控。幾乎就在同時，他也發現這通呼叫來自住在下兩層樓的兒時玩伴。

『……小春，你醒著嗎？』

聽到悄悄從頭部正中央響起的說話聲，春雪不禁有些動搖。因為千百合一向晚上十點就上床睡覺，照理說在她睡到早上七點之前，應該是無論如何都不會醒的。她為什麼會在這種時間醒來，又有什麼事要找自己呢？

春雪將亂成一團的思緒趕到腦海中的角落，以思考發聲含糊地回答：

『我才剛醒過來……』

『風好大耶，不過我會睡不著倒也不是因為風啦。』

『睡不著！你竟然會睡不著！』

春雪忍不住說溜嘴，千百合立刻大喊：『我說你喔！』

『你到底把我當什麼了？而且真要說起來，我會睡不著可都是小春你害的！』

『咦……？是、是因為我……？』

『對啊。你今天……啊，已經是昨天啦？昨天傍晚我要回家的時候，你不就對我說過一些很奇怪的話嗎？說什麼今天晚上也許我會作惡夢，但是神經連結裝置絕對不能拿下，也不能關掉電源。被你這麼一說，會不安得睡不著也理所當然好不好！』

大約十小時前，春雪確實對千百合說過這些話。

理由非常單純，因為對戰格闘遊戲軟體「BRAIN BURST」在安裝完成的第一個晚上，會以惡夢的形式搜尋安裝者的記憶，濾出精神創傷與自卑感等心靈傷痛，創造出安裝者在戰場上的分身「對戰虛擬角色」。

半年前，春雪自己就在獲得「BRAIN BURST」的當晚，作了這輩子最大規模的惡夢。對於惡夢內容他只模模糊糊記得一些，而軟體所創造出來的，就是在極細身體上安了個巨大安全帽頭的銀色虛擬角色「Silver Crow」。

春雪懷念地回想當時自己失望的模樣，對千百合答道：

『那……那有什麼辦法？要是不作惡夢，就創造不出最重要的對戰虛擬角色啊。等等……我現在才忽然想到，你會有什麼精神創傷嗎……』

『你可真敢說！精神創傷這種東西我當然也有的好不好？以前念國小參加遠足的時候，不知道是誰在公車裡玩遊戲玩到平衡感失調，暈車暈得好厲害，最後還在我膝蓋上……』

『對不起，非常抱歉，請你不要再說下去了。』

一激之下反而刺激到自己的精神創傷，讓春雪吶喊著賠罪。然而千百合卻沒有因此就不再翻舊帳，以一種光聽就知道她已經鼓起臉頰的聲調繼續抱怨：

『啊，說到這裡我才發現，當時小春你根本就沒有好好跟我道歉。正好，我現在就要你還我這份人情。』

『咦……咦咦！都幾年前的事情了……追溯期都過了好不好！』

『前陣子電視新聞才剛報過，說追溯期這個詞很快就會變成過去式了。』

的確，隨著記錄日本全國所有公共空間的「公共攝影機網路」整建完成，刑事案件的公訴追溯期限規定在好幾年前就已經全部廢止。不過如果照這樣算來，春雪到底欠了千百合多少人情，可就數也數不清了。

『照「兒時玩伴特別法」規定，追溯期都是只有一年的好不好。』

春雪嘀咕了一會兒，這才同時發出了來自嘴唇的嘆氣與來自腦內的問題。

『……那，你是要我怎麼還你人情？又要請你吃「圓寺屋」的巨無霸聖代？』

『我總覺得最近那家店的味道變差了，多半是把鮮乳換成合成乳了……等等，不是啦。用講的太麻煩了，你馬上給我鑽到我家的家用網路裡面來，我會開好門等你。』

『咦……？』

當春雪為這意想不到的命令驚訝得連連眨眼時，千百合的語音通話已經切斷了。春雪看著通話圖示從閃爍到消失，歪著頭想不通「都幾點了她還想做什麼？」但又沒膽放她鴿子，只好乖乖聽話，開口念出指令：

「直接連線。」

指令才剛念完，昏暗房間內的景象立刻隨著咻的音效呈放射狀淡出，體表感覺跟重力感也被切斷，讓春雪緩緩墜入一片黑暗之中。這是因為開啟了神經連結裝置的「完全沉潛」功能，令他拋下一切其他感覺，只有意識解放到網路之中。

飄浮了一陣子之後，下方有幾個圓形的網站入口緩緩接近，這些地點就是現在可以沉潛的各個網路入口。在全球網路上許多已經加入「我的最愛」的VR空間，以及自家公寓大樓的區域網路之中，就有包括掛著倉嶋家家用網站名牌的入口。春雪就朝著這個入口，伸出了隱形的右手。

緊接著產生一股虛擬引力，將他的意識吸進這小小的入口中。當春雪跳進門內的同時，一個溫暖的檸檬黃光環就在眼前不斷放大——

「嗚……哇。」

看到眼前出現的光景，春雪忍不住叫出聲來。

一般來說，家用網路的VR空間會模仿該家庭住宅的風格來建構，裡頭有「起居室」、「客廳」與每個家庭成員自己的「房間」，而大多數人都會去修改各個房間，調整出現實世界中不可能有的寬廣空間或裝飾來取樂。

然而現在春雪眼中所見，卻是一整片由無數形形色色大小不一的坐墊所構成的海洋。

四面八方都沒有牆壁存在，只見風和日麗的藍天下，蠟筆色調的坐墊堆了又堆，一路延伸到地平線去。春雪就掉落在這片坐墊海的正中央，高高彈起之後又再度落下，一屁股著地坐穩。

「……這、這什麼玩意兒？」

目光掃過躺在正面的黃色長頸鹿型坐墊、旁邊的大象型坐墊，以及再過去的各種奇形怪狀坐墊，讓春雪又一次喃喃自語。

「這是奇蝦，是寒武紀的生物。」

忽然間背後傳來千百合說話的聲音，春雪轉過身去。

一個外型嬌小的虛擬角色，踩在疑似棘冠海星型的坐墊上站著。她全身覆蓋著看起來很柔軟的淡紫色毛皮，還穿了一件小小的連身洋裝。千百合在梅鄉國中校內網路之中，也同樣採用這個看起來像是由貓進化成人的造型作為虛擬角色。

千百合的虛擬角色臉孔有六成像貓，眨著一對水藍色的大眼睛哼了一聲。

「你還在用這個虛擬角色啊？也差不多可以換掉了吧。」

聽她這麼一說，春雪就低頭瞥了自己的身體一眼。

那是具同樣居家學校通用的粉紅豬型虛擬角色。幾乎完全呈球形的軀幹上接著圓滾滾的手腳，臉孔中央突起一個平平的鼻子，另外儘管自己看不到，但頭上應該還長著一對招風耳。

這個模樣實在說不上帥氣或可愛，而且這個虛擬角色其實也不是春雪自己挑的，但他一直拖到現在都還沒有換掉。春雪動動鼻子先呴了一聲，之後才像找藉口似地回答：

「我都已經習慣這個身體的戚覺了，現在才要換掉也很麻煩。而且別說這個了……我剛剛會大喊『這什麼玩意兒』，不是想問你腳下這神秘生物是什麼東西，而是針對這整個VR空間問的。這個坐墊地獄……呃我是說坐墊天堂，到底是怎麼回事？」

他記得千百合從以前就很喜歡這種布偶類坐墊，床上就擺了很多不同的種類，但眼前的規模實在超乎常軌，真不知道這些物件總計用了多少容量。春雪一問之下，貓型虛擬角色就搖著綁有絲帶的尾巴得意地笑了：

「嘻嘻嘻，很棒吧？前陣子為了慶祝我升上二年級，爸媽幫我在家用伺服器里增設了我專用的記憶容量。就算解析度開到這麼高，兩端之間都可以拉到十五公里遠喲。」

「真……真的假的！」

春雪反射性地後退，結果他圓滾滾的屁股一滑，讓整個身體埋進了大象與奇蝦之間。他一邊胡亂掙扎一邊思索，如果自己有這麼多容量，就可以拿去重現一九四三年俄羅斯庫爾斯克坦克大會戰之類的壯大戰場。可以部署成山成海的虎式戰車跟T-34戰車，空中還可以準備Bf109戰鬥機。啊啊，那樣的景象真不知道會多麼令人熱血沸騰。

「……小、小百啊，你這邊也讓我修改一下。」

「不行！」

話才說到一半，千百合就冷漠地拒絕，還從可以看到小小牙齒的嘴裡吐出舌頭。

「要是讓小春修改，一定會弄出那種到處都是汽油、鋼鐵跟煙塵味道的東西。」

「那、那才棒啊。」

「我-才-不-要-啊啊，夠了，害我想講的事情一直都沒機會講。」

春雪抬頭望向將纖細雙臂環抱在胸前的貓型虛擬角色，這才想起自己被叫來的理由。

「啊……對、對喔。那我到底得做什麼？」

「你只要坐在那邊就好了。」

「咦？」

坐在巨大坐墊上的春雪不明所以，維持短短雙腿往前伸出的姿勢歪頭思索。緊接著——

貓型虛擬角色輕飄飄地跳到他眼前，修長的身體毫不猶豫地躺到春雪腳上。

「嗚……哇？」

春雪彈跳起來，想要往上鑽出出口，但千百合的右手卻猛然抓住他的鼻子，強行將他拉回原位。

「你啊，給我乖乖躺在那邊當枕頭，這樣我就忘記遠足時的事。丑話說在前面，你要是敢動歪腦筋，我就放奇蝦咬你。」

「我、我才不會啦！不對……你說枕頭，是怎麼……」

千百合沒回答春雪的問題，彈響了長著小小爪子的手指。緊接著頭上風和日麗的藍天就整個開始旋轉，切換成浮著巨大月亮的夜空。

無數只會在圖畫故事書上看到的星型天體，鳴響著鈴鐺般的音效。她就在這些星星的閃爍下——也在春雪的膝上——大大打了個呵欠，接著才側躺著縮起身體。

「……沒有什麼特別的意思啦。」

從春雪的角度看不見的嘴，小聲地嘟囔著。

「我只是想起從前，小春你還常來我們家過夜的時候，只要拿你當枕頭，我都可以很快睡

「……你、你在講幾百年前的事情啊……」

「誰知道呢？都已經好久……好~久了。」

貓型虛擬角色打了個呵欠，真的閉上了眼睛。

……這種事情你應該去拜託阿拓吧。

春雪本想這麼說，但隨即吞下了這句話。小時候曾經當枕頭給千百合躺的人，就只有春雪一個。拓武由於雙親的家教十分嚴格，所以從來不曾在春雪或千百合家裡過夜。

然而就算是這樣，這種好久以前的條件反射習慣會一直留到現在嗎？而且現在雙方都以動物型的虛擬角色相見，所在的地方更是神經連結裝置所營造出的虛擬坐墊天堂。不過話又說回來，以血肉之軀絕對做不出這種事來。不，就算是VR恐怕也未必可以。

這些念頭在腦子裡轉著轉著，就發現千百合居然真的開始發出平穩的打呼聲。

「……真是的……」

春雪忍不住嘀咕，原以為已經睡著的千百合卻緊接著以含糊不清的聲調輕聲說：

「小春，你知道嗎……我啊，真的好努力喲……」

「咦？努力什麼……？」

「為了當上超頻連線者……我真的好努力好努力……這樣一來，我們，又可以，回到那個時候了，對吧……回到我們三個，每天都一起玩到天黑，的那個……時候……」

千百合說到這裡，似乎就真的墜入了深沉的夢鄉。春雪右手輕輕摸著發出虛擬打呼聲的虛擬體耳下柔軟的毛皮，默默在心中回答。

——有些東西是絕對不會改變的。

——可是多半也有些東西一旦改變，就再也沒辦法恢復了。

幾分鐘後，神經連結裝置偵測到千百合進入深沉的睡眠狀態，自動解除了完全沉潛的功能。貓型虛擬角色隨著鈴聲般的聲響從膝蓋上消失後，春雪仍然在這群不說話的動物之間癱坐了好一會兒。
