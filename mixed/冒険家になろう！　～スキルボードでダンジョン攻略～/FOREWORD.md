# 角色紹介

#### 空星晴輝

![空星晴輝](https://neo.usachannel.info/wp-content/uploads/2019/03/0021.png)

初心者冒険者で自宅にダンジョンを持っている。人から認識されないほど存在感が薄い

#### 紅連

![紅連](https://neo.usachannel.info/wp-content/uploads/2019/03/0093.png)

仲間の冒険者に囮として取り残されモンスターパレードに巻き込まれていたところを晴輝に助けられる。

#### 夕月朱音

![夕月朱音](https://neo.usachannel.info/wp-content/uploads/2019/03/0068.png)

悪徳っぽい武器屋、気に食わないお客にスープレックスをお見舞いしたり、晴輝に呪いの仮面を売りつけたりしている。

#### 大井素

![大井素](https://neo.usachannel.info/wp-content/uploads/2019/03/0123.png)

モンスター素材買取店の店員、夕月とはライバル関係にありそう、見た目通り真面目
